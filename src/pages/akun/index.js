import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  StyleSheet,
  ScrollView,
  ImageBackground,
  StatusBar, Alert
} from 'react-native';
import { header } from '../../assets';


const Akun= ({navigation}) => {

  const [kategori, setKategori] = useState([
    {
      nama: 'Puisi',
    },
    {
      nama: 'Cerpen',
    },
    {
      nama: 'Karya Ilmiah',
    },
    {
      nama: 'Quotes',
    },
    {
      nama: 'Informasi',
    },
  ]);
  
  const [kategoriSeleksi, setKategoriSeleksi] = useState({
    nama: 'Puisi',
  });

  const [daftarRekomendasi, setDaftarRekomendasi] = useState([
    {
      judul: 'PAB',
      image: require('../../assets/images/logo.png'),
      deskripsi: 'PAB angkatan 22',
    },
    {
      judul: 'Follow Up',
      image: require('../../assets/images/logo.png'),
      deskripsi: 'Follow Up PAB 22',
    },
    {
      judul: 'Kantin',
      image: require('../../assets/images/logo.png'),
      deskripsi: 'Kajian Rutin UKM-PI',
    },
    {
      judul: 'Pelatihan',
      image: require('../../assets/images/logo.png'),
      deskripsi: 'Pelatihan UKM-PI',
    },
  ]);

  return (
    <View style={{flex: 1, backgroundColor: '#8FDAEE',}}>
      <ScrollView>
        <StatusBar backgroundColor="#8FDAEE" barStyle="light-content" />
        <View style={{marginHorizontal: 20, alignItems: 'center', marginTop: 20}}>
              <Text style={{fontSize: 30, fontWeight: 'bold', color: '#FFFFFF'}}>
                 kegiatan UKM-Pi
            </Text>
          </View>
      <View style={{flex: 1}}>
        <View>
        <View style={{marginHorizontal: 2, marginBottom: 20, marginTop: 20}}>
            <ImageBackground source={ header } style={styles.header}>
            </ImageBackground>
        </View>
        <View>
          <FlatList
            data={kategori}
            horizontal
            showsHorizontalScrollIndicator={false}
            style={{marginLeft: 10}}
            renderItem={({item}) => (
              <TouchableOpacity
                style={{
                  marginRight: 5,
                  backgroundColor:
                    kategoriSeleksi.nama == item.nama ? '#4169e1' : '#fff',
                  elevation: 3,
                  paddingHorizontal: 15,
                  paddingVertical: 8,
                  marginBottom: 10,
                  borderRadius: 15,
                  marginLeft: 5,
                }}
                onPress={() => setKategoriSeleksi(item)}>
                <Text
                  style={{
                    color:
                      kategoriSeleksi.nama == item.nama ? '#fff' : '#212121',
                  }}>
                  {item.nama}
                </Text>
              </TouchableOpacity>
            )}
          />
        </View>
        <View
          style={{
            marginHorizontal: 20,
            marginBottom: 10,
            marginTop: 20,
            flexDirection: 'row',
          }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 18, fontWeight: 'bold', color: '#212121'}}>
              Kegiatan-kegiatan UkM_PI UIM
            </Text>
          </View>
        </View>
          <View style={{marginLeft: 50, marginTop: 10}}>
            <FlatList     
              data={daftarRekomendasi}
              renderItem={({item}) => (
                <TouchableOpacity onPress={() => Alert.alert('Penting','Maaf ini hanya sampul puisi belum tersedia.')}
                  style={{
                    backgroundColor: '#FFFFFF',
                    alignItems: 'center',

                    marginTop: 10,
                    flexDirection: 'column-reverse',
                    marginRight: 50,
                    elevation: 3,
                    padding: 10,
                    marginBottom: 8,
                    borderRadius: 5,
                  }}>
                  <View style={{margincolumn: 10, width: 200, alignItems: 'center',}}>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        fontSize: 20,
                        color: '#212121',
                      }}>
                      {item.judul}
                    </Text>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        fontSize: 20,
                        color: '#A5A5A5',
                      }}>
                      {item.deskripsi}
                    </Text>
                    <Text style={{fontSize: 14, alignItems: 'center',}}>{item.deskripsi}</Text>
                    
                  </View>
                  <View>
                    <Image
                      source={item.image}
                      style={{width: 190, height: 200, borderRadius: 5 }}
                      resizeMode="contain"
                    />
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
        </View>
      </View>
      </ScrollView>
    </View>
  );
};

export default Akun;
const styles = StyleSheet.create({
  

  header: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 386,
    height:186
  },

})