import { ImageBackground, StyleSheet, Image } from 'react-native'
import React, {useEffect} from 'react'
import { puisi, logo } from '../../assets'


const Splash = ({navigation}) => {

  useEffect(() =>{
    setTimeout(() => {
      navigation.replace('MainApp');
    }, 6000);
  }, [navigation]);

  return (
    <ImageBackground source={puisi} style={styles.background}>
      <Image source={logo} style={styles.logo} />
    </ImageBackground>
  )
}

export default Splash

const styles = StyleSheet.create({
  background: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  logo: {
    width: 222,
    height:222
  }

})