import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  StatusBar,
  Image,
  ScrollView,
  ImageBackground,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { ukmPI } from '..';
import { header } from '../../assets';
const UkmPI= () => {
  const [kategori, setKategori] = useState([
    {
      nama: 'Puisi',
    },
    {
      nama: 'Cerpen',
    },
    {
      nama: 'Karya Ilmiah',
    },
    {
      nama: 'Quotes',
    },
    {
      nama: 'Kegiatan',
    },
    {
      nama: 'Informasi',
    },
  ]);

  const [kategoriSeleksi, setKategoriSeleksi] = useState({
    nama: 'Puisi',
  });

  const [dataTrending, setDataTrending] = useState([
    {
      judul: 'UKM-PI',
      author: 'Sejara UKM-PI',
      Image: require('../../assets/images/logo.png'),
    },
    {
      judul: 'Logo',
      author: 'Makna Logo',
      Image: require('../../assets/images/logo.png'),
    },
    {
      judul: 'Pengurus',
      author: 'Struktural Kepengurusan',
      Image: require('../../assets/images/logo.png'),
    },
    {
      judul: 'Proker',
      author: 'Proker Kepengurusan',
      Image: require('../../assets/images/logo.png'),
    },
   
    
  ]);

  const [dataKegiatan, setDataKegiatan] = useState([
    {
      judul: 'UKM-PI',
      author: 'Sejara UKM-PI',
      Image: require('../../assets/images/logo.png'),
    },
    {
      judul: 'Logo',
      author: 'Makna Logo',
      Image: require('../../assets/images/logo.png'),
    },
    {
      judul: 'Pengurus',
      author: 'Struktural Kepengurusan',
      Image: require('../../assets/images/logo.png'),
    },
    {
      judul: 'Proker',
      author: 'Proker Kepengurusan',
      Image: require('../../assets/images/logo.png'),
    },
   
    
  ]);

  return (
    <View style={{flex: 1, backgroundColor: '#8FDAEE'}}>
      <ScrollView>
        <StatusBar backgroundColor="#8FDAEE" barStyle="dark-content" />
        <View style={{marginHorizontal: 2, marginBottom: 20, marginTop: 20}}>
            <ImageBackground source={ header } style={styles.header}>
            </ImageBackground>
        </View>
        <View>
          <FlatList
            data={kategori}
            horizontal
            showsHorizontalScrollIndicator={false}
            style={{marginLeft: 10}}
            renderItem={({item}) => (
              <TouchableOpacity
                style={{
                  marginRight: 5,
                  backgroundColor:
                    kategoriSeleksi.nama == item.nama ? '#4169e1' : '#fff',
                  alignItems: 'center',
                  elevation: 3,
                  paddingHorizontal: 15,
                  paddingVertical: 8,
                  marginBottom: 10,
                  borderRadius: 15,
                  marginLeft: 5,
                }}
                onPress={() => setKategoriSeleksi(item)}>
                <Text
                  style={{
                    color:
                      kategoriSeleksi.nama == item.nama ? '#fff' : '#212121',
                  }}>
                  {item.nama}
                </Text>
              </TouchableOpacity>
            )}
          />
        </View>
        <View
          style={{
            marginHorizontal: 60,
            marginBottom: 20,
            marginTop: 18,
            flexDirection: 'row',
          }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',

            }}>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: '#212121', justifyContent: 'center', textAlign: 'center'}}>
              UKM-Pengembangan Intelektual
            </Text>
          </View>
        </View>

        <View>
          <FlatList
            data={dataTrending}
            horizontal
            showsHorizontalScrollIndicator={false}
            style={{marginLeft: 10}}
            renderItem={({item}) => (
              <TouchableOpacity
                style={{
                  marginRight: 5,
                  backgroundColor: '#fff',
                  elevation: 3,
                  paddingHorizontal: 15,
                  paddingVertical: 8,
                  marginBottom: 10,
                  borderRadius: 15,
                  marginLeft: 5,
                }}>
                <Image
                  source={item.Image}
                  style={{
                    width: 170,
                    height: 170,
                    marginTop: 10,
                    marginBottom: 10,
                    borderRadius: 3,
                  }}
                  resizeMode={'stretch'}
                />
                <Text
                  style={{
                    color: '#212121',
                    fontSize: 18,
                    fontWeight: 'bold',
                    justifyContent: 'center',
                    alignContent: 'center',
                    alignItems: 'center'
                  }}>
                  {item.judul}

                </Text>
                <Text
                  style={{
                    color: '#A5A5A5',
                    fontSize: 18,
                    fontWeight: 'bold',
                  }}>
                  {item.author}
                </Text>
              </TouchableOpacity>
            )}
          />
        </View>

        <View
          style={{
            marginHorizontal: 20,
            marginBottom: 10,
            marginTop: 20,
            flexDirection: 'row',
          }}>
        </View>

        

      </ScrollView>
    </View>
  );
};

export default UkmPI;
const styles = StyleSheet.create({
  

  header: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 386,
    height:186
  },

})