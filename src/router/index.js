import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { home, splash, ukmPI, akun } from '../pages';



const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
    return (
      
      <Tab.Navigator
      initialRouteName='Home'
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, size, color}) => {
          let iconName;
          if (route.name == 'Home'){
            iconName = focused ? 'Home' : 'Home';
          } else if (route.name == 'UkmPI'){
            iconName = focused ? 'UkmPI' : 'UkmPI';
          } else if (route.name == 'Akun'){
            iconName = focused ? 'Akun' : 'Akun';
          } 
           return <Icon name={iconName} size={size} colour={color}/>
        },
      })}> 
        <Tab.Screen name="Home" component={home} options={{headerShown: false}} />
        <Tab.Screen name="UKM-PI" component={ukmPI} options={{headerShown: false}}  />
        <Tab.Screen name="Kegiatan" component={akun} options={{headerShown: false}} />

      </Tab.Navigator>
    )
  }

const Router = () => {
  return (
    
      <Stack.Navigator initialRouteName='splash'>
        <Stack.Screen name="splash" component={splash} options={{ headerShown: false}}/>
        <Stack.Screen name="MainApp" component={MainApp} options={{ headerShown: false}}/>
      </Stack.Navigator>
 
  )
}


export default Router;

const styles = StyleSheet.create({})